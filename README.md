# lirc2serial
forward LIRC commands to a serial (RS-232) connection

##About

lirc2serial allows forwarding infrared signals received via LIRC to a serial (RS-232) interface.

Its intended purpose is to control some Hifi devices (e.g. a [Devialet amplifier][1]) by reading commands 
from a LIRC socket and forwarding them to the device's serial port.

Requires LIRC, pylirc, pyserial

##Usage

`lirc2serial [options]...`

##Options

* **-c**, **--config**  
the config file to use
* **-h**, **--help**  
Show help message and exit
* **-l LEVEL**, **--log LEVEL**  
the logging level (DEBUG, INFO, WARNING, ERROR, CRITICAL)
* **-r**, **--lircrc**  
the lircrc file to use
* **-s**, **--stdin**  
read from stdin instead of lirc socket (for testing purposes)
* **-v**, **--version**  
print the program version and exit

##Dependencies

lirc2serial requires [LIRC][2], [pylirc][3], and [pyserial][4].

##Author

Copyright 2015 Daniel James <dj (at) slashspace (dot) org>

##License

lirc2serial is released under the GNU General Public License, version 3 or later (see LICENSE).

[1]: http://www.devialet.com/docs/Automation-Devialet-notice.pdf
[2]: http://www.lirc.org/
[3]: http://pylirc.mccabe.nu/
[4]: https://pypi.python.org/pypi/pyserial