'''
Created on 8 Nov 2015

@author: daniel
'''

from serial import Serial
from sys import stdout
  

class SerialWriter(object):
    '''
    classdocs
    '''

    def __init__(self, port, baudrate, parity, bytesize, stopbits, timeout):
        self._ser = Serial(port, 
                           baudrate, 
                           parity=parity, 
                           bytesize=bytesize, 
                           stopbits=stopbits, 
                           timeout=timeout)
    
    def writecode(self, s):
        self._ser.write(s)
        self._ser.flush()
        
    def readln(self):
        return self._ser.readall()

class StdoutWriter(object):
    
    def __init__(self):
        pass
    
    def writecode(self, s):
        stdout.write(s)
        