#!/usr/bin/env python2.7
# encoding: utf-8
"""
lirc2serial -- forward LIRC commands to a serial (RS-232) connection

lirc2serial allows forwarding infrared signals received via LIRC to a serial (RS-232) interface.

Its intended purpose is to control some hifi devices (e.g. a Devialet amplifier) by reading commands 
from a LIRC socket and forwarding them to the device's serial port.

Requires LIRC, pylirc, pyserial


@author:     Daniel James

@copyright:  2015 Daniel James. All rights reserved.

@license:    GNU General Public License, Version 3 or later

@contact:    dj (at) slashspace (dot) org

@deffield    updated: 2015-12-12
"""

from ConfigParser import SafeConfigParser
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

import logging as _log
import reader
import sys as _sys
import writer


__version__ = "0.0.1"
__updated__ = "2015-12-12"

DEBUG = 0

def add_args(parser):
    parser.add_argument("-l", "--log", metavar="LEVEL", default="WARNING",
                        help="The logging level (DEBUG, INFO, WARNING, ERROR, CRITICAL)")
    parser.add_argument("-s", "--stdin", action="store_true",
                        help="Read from stdin instead of lirc socket (for testing purposes)")
    parser.add_argument("-v", "--version", action="version", version=__version__,
                        help="Print the program version and exit")
    parser.add_argument("-c", "--config", metavar="FILE", default="./lirc2serial.conf",
                        help="The config file to use")
    parser.add_argument("-r", "--lircrc", metavar="FILE", default="./lirc2serial.lircrc",
                        help="The lircrc file to use")

def main(argv=None):
    """Command line options."""

    if argv is None:
        argv = _sys.argv
    else:
        _sys.argv.extend(argv)
    
    parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter)
    add_args(parser)
    args = parser.parse_args()
    
    _log.basicConfig(format="%(levelname)s:%(message)s", level=args.log.strip())
    _log.info("Logging level set to '{}'".format(args.log.strip()))
    
    cfgparser = SafeConfigParser()
    cfgparser.read(args.config)
    
    rdr = reader.StdinReader() if args.stdin else reader.LircReader(cfgparser.get("lircrc", "program"), args.lircrc)
    wtr = writer.SerialWriter(cfgparser.get("serial", "port"), 
                              cfgparser.get("serial", "baudrate"), 
                              cfgparser.get("serial", "parity"), 
                              cfgparser.getint("serial", "bytesize"), 
                              cfgparser.getint("serial", "stopbits"), 
                              cfgparser.getfloat("serial", "timeout"))
    
    try:
        while 1:
            codes = rdr.readnext()
            for c in codes:
                _log.debug(">>>: " + c)
                wtr.writecode(c)
    except KeyboardInterrupt:
        _log.debug("Termination signal caught")
        print
        return 0

if __name__ == "__main__":
    if DEBUG:
        _sys.argv.append("-l DEBUG")
    _sys.exit(main())
