'''
Created on 8 Nov 2015

@author: daniel
'''

from abc import ABCMeta, abstractmethod
from sys import stdin
import pylirc
import os

class AbcReader(object):
    '''
    classdocs
    '''
    __metaclass__ = ABCMeta

    def __init__(self):
        pass
        
    @abstractmethod
    def readnext(self):
        pass
        
class StdinReader(AbcReader):
    '''
    classdocs
    '''
    
    def __init__(self):
        super(StdinReader, self).__init__()
    
    def readnext(self):
        ln = stdin.readline().rstrip(os.linesep)
        return [ ln ] 
        
class LircReader(AbcReader):
    '''
    classdocs
    '''
    
    def __init__(self, progname, lircrc):
        super(LircReader, self).__init__()
        self._progname = progname
        self._lircrc = lircrc
        pylirc.init(progname, lircrc, 1)
    
    def readnext(self):
        buttons = []
        codes = pylirc.nextcode(1)
        if codes:
            for code in codes:
                if code["config"]:
                    buttons.append(code["config"])
        return buttons
    